class produits {
  constructor(prod, cal, prot, carb, lipid) {
    this.prod = prod;
    this.cal = cal;
    this.prot = prot;
    this.carb = carb;
    this.lipid = lipid;
  }
  get Getprod(){
    return this.prod;
  }
  get Getcal(){
    return this.cal;
  }
  get Getpprot(){
    return this.prot;
  }
  get Getcarb(){
    return this.carb;
  }
  get Getlipid(){
    return this.lipid;
  }
}

var Tableprod=Array();
var nbrlignetableprod=0;
var k=0;


function Addtorepas(node){
	/*======== Recuper la ligne de ce tableau ============*/
	//alert('daz');
	nbrlignetableprod++;
	var ligne=node.parentNode.parentNode.parentNode.parentNode.parentNode;
	var inputtext=ligne.getElementsByTagName('input')[0];
	var valeur=parseFloat(inputtext.value);
	/*===== Recuperer les valeurs de tableau =====*/ 
	if(isNaN(valeur)){
		alert("Veuillez ajouter une valeur de votre aliment");
		inputtext.style.borderColor="#d43f3a";
	}
	else
	{
		inputtext.style.borderColor="";
		var prod=ligne.cells[0].getElementsByTagName('img')[0].alt+"("+valeur+"g)";
		var cal =ligne.cells[1].getElementsByTagName('span')[0].innerHTML;
		var prot =ligne.cells[2].getElementsByTagName('span')[0].innerHTML;
		var carb =ligne.cells[3].getElementsByTagName('span')[0].innerHTML;
		var lipid =ligne.cells[4].getElementsByTagName('span')[0].innerHTML;
		
		/*=== remplir tableprod ====*/
		
		/*===== Tableau resultat =====*/
		/*============================*/
		var calG;
		var protG;
		var carbG;
		var lipidG;
		var tableR =document.getElementById("table-resultat");

		/*==== Recuper les valeurs actuels =====*/
		
		if(tableR.rows[2].cells[1].innerHTML==''){
			calG=cal;
			protG=prot;
			carbG=carb;
			lipidG=lipid;
		}
		else{
			calG=tableR.rows[2].cells[1].innerHTML;
			protG=tableR.rows[2].cells[2].innerHTML;
			carbG=tableR.rows[2].cells[3].innerHTML;
			lipidG=tableR.rows[2].cells[4].innerHTML;
			/*=== ====*/
			cal=(parseFloat(cal)*parseFloat(valeur))/100;
			prot=(parseFloat(prot)*parseFloat(valeur))/100;
			carb=(parseFloat(carb)*parseFloat(valeur))/100;
			lipid=(parseFloat(lipid)*parseFloat(valeur))/100;


			calG=Math.round((parseFloat(calG)+parseFloat(cal))* 100) / 100;
			protG=Math.round((parseFloat(protG)+parseFloat(prot))* 100) / 100;
			carbG=Math.round((parseFloat(carbG)+parseFloat(carb))* 100) / 100;
			lipidG=Math.round((parseFloat(lipidG)+parseFloat(lipid))* 100) / 100;
		}
		

		/*==== Remplir le tableau Resultat ====*/
		tableR.rows[2].cells[0].innerHTML+="<div id='button-resultat'><button width='10px' height='10px' type='button' onclick='Deletefromrepas(this);'>x</button><span>"+prod+"</span></div>";
		tableR.rows[2].cells[0].getElementsByTagName('span')[k].style.marginLeft="10%";
		tableR.rows[2].cells[0].getElementsByTagName('div')[k].style.marginBottom="2%";
		tableR.rows[2].cells[0].getElementsByTagName('button')[k].style.background="#d9534f";
		tableR.rows[2].cells[0].getElementsByTagName('button')[k].style.color="#fff";
		tableR.rows[2].cells[0].getElementsByTagName('button')[k].style.borderColor="#d43f3a";
		tableR.rows[2].cells[0].getElementsByTagName('button')[k].style.border="1px solid #c1c1c138";
		tableR.rows[2].cells[0].getElementsByTagName('button')[k].style.borderRadius="4px";

		tableR.rows[2].cells[1].innerHTML=calG;
		tableR.rows[2].cells[2].innerHTML=protG;
		tableR.rows[2].cells[3].innerHTML=carbG;
		tableR.rows[2].cells[4].innerHTML=lipidG;
		/*=== cree objet produit ====*/
		var ligneproduit = new produits(prod,cal,prot,carb,lipid);
		/*=== instancié tableprod ====*/
		Tableprod.push(ligneproduit);
		k++;
		//alert(x);
	}
	return false;
}

function Deletefromrepas(node){	
	/*=== les valeurs recuperer du table ===*/
	var rowfound=Searchprod(node);
	var tablegenerale =new Array();
	var tablenode =new Array();
	tablegenerale[0] =Math.round(rowfound[0]* 100) / 100;
	tablegenerale[1] =Math.round(rowfound[1]* 100) / 100;
	tablegenerale[2] =Math.round(rowfound[2]* 100) / 100;
	tablegenerale[3] =Math.round(rowfound[3]* 100) / 100;

	var tableR =document.getElementById("table-resultat");
	tablenode[0]=tableR.rows[2].cells[1].innerHTML;
	tablenode[1]=tableR.rows[2].cells[2].innerHTML;
	tablenode[2]=tableR.rows[2].cells[3].innerHTML;
	tablenode[3]=tableR.rows[2].cells[4].innerHTML;
	

	tablenode[0]=Math.round((tablenode[0]-tablegenerale[0])* 100) / 100;
	tablenode[1]=Math.round((tablenode[1]-tablegenerale[1])* 100) / 100;
	tablenode[2]=Math.round((tablenode[2]-tablegenerale[2])* 100) / 100;
	tablenode[3]=Math.round((tablenode[3]-tablegenerale[3])* 100) / 100;

	//console.log(tablenode);

	tableR.rows[2].cells[1].innerHTML=tablenode[0];
	tableR.rows[2].cells[2].innerHTML=tablenode[1];
	tableR.rows[2].cells[3].innerHTML=tablenode[2];
	tableR.rows[2].cells[4].innerHTML=tablenode[3];


	var button=node.parentNode;
	button.parentNode.removeChild(button);
	k--;
}


function Searchprod(node){
	
	/*===== Recuperer nom produit =====*/
	var prod=node.parentNode.getElementsByTagName('span')[0].innerHTML;
	/*====== ======*/
	
	var resultatfound=new Array();
	/*var table=document.getElementsByTagName("table")[1];
	var row=table.getElementsByClassName("rows-full");*/
	var lenght=Tableprod.length;
	//console.log(table.getElementsByClassName("rows-full")[1].cells[0].getElementsByTagName('img')[0].alt);
	/*==== =====*/
	var i=0;
	//console.log(prod);
	while(i<lenght){
		//var rowfound=table.getElementsByClassName("rows-full")[i];Tableprod
		var rowfound=Tableprod[i];
		if(rowfound['prod']==prod){
			resultatfound[0]=rowfound['cal'];
			resultatfound[1]=rowfound['prot'];
			resultatfound[2]=rowfound['carb'];
			resultatfound[3]=rowfound['lipid'];
			return resultatfound;
		}
		i++;
	}
}
function Empty(node){
	var row=node.parentNode.parentNode.parentNode.parentNode;
	row.cells[0].innerHTML='';
	row.cells[1].innerHTML=0;
	row.cells[2].innerHTML=0;
	row.cells[3].innerHTML=0;
	row.cells[4].innerHTML=0;
	k=0;
}
