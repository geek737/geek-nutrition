<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include "conn.php";
$req="SELECT * from fruits";
$res=$conn->query($req);
$data=$res->fetchAll();
$table=array('name'=>'','picture'=>'','Calories'=>'','Proteins'=>'','Carbohydrates'=>'','Lipids'=>'');
echo"	
			<div class='comparison '>	
			  <table>
			    <thead>
			      <tr>
			        <th class='qbse text-center'>Pour 100 g de produit</th>
			        <th class='qbse text-center'>
			          Calories (kcal)
			        </th>
			        <th class='qbse text-center'>
			          Proteins (g)
			        </th>
			        <th class='qbse text-center'>
			          Carbohydrates (g)
			        </th>
			        <th class='qbse text-center'>
			          Lipids (g)
			        </th>
			        <th colspan='2' class='qbse text-center'>
			          ADD
			        </th>
			      </tr>
			    </thead>
			    <tbody>";
			    foreach ($data as $row) {
			    	$nom=$row['name']; 
			    	$picture=$row['picture'];
			    	$Calories=$row['calories'];
			    	$Proteins=$row['proteins'];
			    	$Carbohydrates=$row['carbohydrates'];
			    	$Lipids=$row['Lipids'];
			    echo"<tr>
		        	<td></td>
		        	<td colspan='6'>$nom</td>
		      	</tr>
		      	<tr class='rows-full'>
			        <td><img class='image-tableau'src='image/fruits/$picture' height='100px' width='100px' alt='$nom'></td>
			        <td><span class='tickblue'>$Calories</span></td>
			        <td><span class='tickblue'>$Proteins</span></td>
			        <td><span class='tickblue'>$Carbohydrates</span></td>
			        <td><span class='tickblue'>$Lipids</span></td>
			        <td colspan='2'>
						<div class='input-group mb-3'>
						<input  placeholder='e.g:100' min='0' on class='form-control' type='number'/>
						  	<div class='input-group-append' required>
						    	<span class='input-group-text'><button type='submit' class='btn btn-info' onclick='Addtorepas(this);'>+</button></span>
						  	</div>
						</div>
					</td>
		      	</tr>";
			  }
			    echo"</tbody>
			    </table>
			</div>";
