<?php
class Function_calcul{
	//Attributs
	private $height;
	private $weight;
	private $age;
	private $sexe;
	//constructor

	public function __construct($height,$weight,$age,$sexe){
		$this->height=$height;
		$this->weight=$weight;
		$this->age=$age;
		$this->sexe=$sexe;
	}
	public function Calcul_BMR(){
		$BMR=0;
		$roundBMI=0;
		if($this->sexe=="homme"){
			$bodyHeight = $this->height*5.003;
			$bodyWeight = $this->weight*13.75;
			$bodyAge=$this->age*6.755;
			$BMR = 66.47;
		}
		else if($this->sexe=="femme"){
			$bodyHeight = $this->height*9.563;
			$bodyWeight = $this->weight*1.85;
			$bodyAge=$this->age*4.676;
			$BMR =655.1;
		}
		return round($BMR+$bodyHeight+$bodyWeight-$bodyAge);
	}
	public function Calcul_BMI(){
		$bodyHeight = $this->height * 0.01;
		$BMI = $this->weight/($bodyHeight*$bodyHeight);
		$roundBMI=round($BMI* 100) / 100;
		return $roundBMI;
	}
	public function BMI_message($roundBMI){
		$table=array('message'=>'','color'=>'');
		if($roundBMI<16){
			$table['message']="Vous êtes trés maigre.";
			$table['color']="#ef8332";
		}
		else if($roundBMI>=16&&$roundBMI<18.5){
			$table['message']="Vous êtes maigre.";
			$table['color']="#f3ae3d";
		}
		else if($roundBMI>=18.5&&$roundBMI<=24.9){
			$table['message']="Vous êtes dans la norme.";
			$table['color']="#66c543";
		}
		else if($roundBMI>=25&&$roundBMI<=29.9){
			$table['message']="Vous êtes proche du surpoids mais encore dans la norme.";
			$table['color']="#f3ae3d";
		}
		else if($roundBMI>=30&&$roundBMI<=39.9){
			$table['message']="Vous êtes en surpoids.";
			$table['color']="#ef8332";
		}
		else if($roundBMI>=40){
			$table['message']="Vous êtes légèrement en surpoids.";
			$table['color']="#ea3223";
		}
		return $table;
	}
}
?>