<?php
include "conn.php";
$req="SELECT * from legumes";
$res=$conn->query($req);
$data=$res->fetchAll();
$table=array('name'=>'','picture'=>'','Calories'=>'','Proteins'=>'','Carbohydrates'=>'','Lipids'=>'');
echo"	
			<div class='comparison '>	
			  <table>
			    <thead>
			      <tr>
			        <th class='qbse text-center'>Pour 100 g de produit</th>
			        <th class='qbse text-center'>
			          Calories (kcal)
			        </th>
			        <th class='qbse text-center'>
			          Proteins (g)
			        </th>
			        <th class='qbse text-center'>
			          Carbohydrates (g)
			        </th>
			        <th class='qbse text-center'>
			          Lipids (g)
			        </th>
			        <th class='qbse text-center' colspan='2'>
			          ADD
			        </th>
			      </tr>
			    </thead>
			    <tbody>";
			    foreach ($data as $row) {
			    	$nom=$row['name']; 
			    	$picture=$row['picture'];
			    	$Calories=$row['calories'];
			    	$Proteins=$row['proteins'];
			    	$Carbohydrates=$row['carbohydrates'];
			    	$Lipids=$row['Lipids'];
			    echo"<tr>
		        	<td></td>
		        	<td colspan='5'>$nom</td>
		      	</tr>
		      	<tr class='rows-full'>
			        <td><img class='image-tableau'src='image/legumes/$picture' height='100px' width='100px' alt='$nom'></td>
			        <td><span class='tickblue'>$Calories</span></td>
			        <td><span class='tickblue'>$Proteins</span></td>
			        <td><span class='tickblue'>$Carbohydrates</span></td>
			        <td><span class='tickblue'>$Lipids</span></td>
			        <td colspan='2'>
						<div class='input-group mb-3'>
						<input placeholder='e.g:100' min='0' class='form-control' type='text'/>
						  	<div class='input-group-append' required>
						    	<span class='input-group-text'><button type='submit' class='btn btn-info'onclick='Addtorepas(this);'>+</button></span>
						  	</div>
						</div>
					</td>
		      	</tr>";
			  }
			    echo"</tbody>
			    </table>
			</div>";