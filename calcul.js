	
	function Resultat(){

	var result=document.getElementById("bmiResultat");
	var height=document.getElementById("height").value;
	var weight=document.getElementById("weight").value;
	var bodyHeightInMeter = height * 0.01;
	var BMI = weight / (bodyHeightInMeter * bodyHeightInMeter);
	var roundBMI=Math.round(BMI * 100) / 100;
	result.innerHTML=roundBMI;
		if(roundBMI<16){
			result.innerHTML=result.innerHTML+'<br> Vous êtes trés maigre.';
			result.style.color="red";
		}
		else if(roundBMI>=16&&roundBMI<18.5){
			result.innerHTML=result.innerHTML+'<br> Vous êtes maigre.';
			result.style.color="orange";
		}
		else if(roundBMI>=18.5&&roundBMI<=24.9){
			result.innerHTML=result.innerHTML+'<br> Vous êtes dans la norme.';
			result.style.color="green";
		}
		else if(roundBMI>=25&&roundBMI<=29.9){
			result.innerHTML=result.innerHTML+'<br> Vous êtes proche du surpoids mais encore dans la norme.';
			result.style.color="yellow";
		}
		else if(roundBMI>=30&&roundBMI<=39.9){
			result.innerHTML=result.innerHTML+'<br> Vous êtes en surpoids.';
			result.style.color="orange";
		}
		else if(roundBMI>=40){
			result.innerHTML=result.innerHTML+'<br> Vous êtes légèrement en surpoids.';
			result.style.color="red";
		}
		return false;
}
/*(function ($) {
	var bmiCal = $('.bmical');
	bmiCal.on('submit',
		function(e){
		//e.preventDefault();
		var $this = $(this),
			$result = $this.find('.bmi_result'),
			bmicm = parseInt($this.find('input.feet').val(), 10),
			bodyHeightInMeter = bmicm * 0.01,
			bmikg = parseInt($this.find('.weight').val(), 10),
			BMI = bmikg / (bodyHeightInMeter * bodyHeightInMeter),
			roundBMI=Math.round(BMI * 100) / 100;
		$result.html(roundBMI);
		if(roundBMI<16){
			$result.html($result.html()+'<br> Vous êtes trés maigre.');
			$result.css({ "color" : "red" });
		}
		else if(roundBMI>=16&&roundBMI<18.5){
			$result.html($result.html()+'<br> Vous êtes maigre.');
			$result.css({ "color" : "orange" });
		}
		else if(roundBMI>=18.5&&roundBMI<=24.9){
			$result.html($result.html()+'<br> Vous êtes dans la norme.');
			$result.css({ "color" : "green" });
		}
		else if(roundBMI>=25&&roundBMI<=29.9){
			$result.html($result.html()+'<br> Vous êtes proche du surpoids mais encore dans la norme.');
			$result.css({ "color" : "yellow" });
		}
		else if(roundBMI>=30&&roundBMI<=39.9){
			$result.html($result.html()+'<br> Vous êtes en surpoids.');
			$result.css({ "color" : "orange" });
		}
		else if(roundBMI>=40){
			$result.html($result.html()+'<br> Vous êtes légèrement en surpoids.');
			$result.css({ "color" : "red" });
		}
		return false;
	});

	
})(jQuery);*/