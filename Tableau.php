<?php
echo"<!DOCTYPE html>
<html>
<head>
	<title>BMI</title>
	<meta charset='utf-8'>
			<!-- fonts -->
		<link href='https://fonts.googleapis.com/css?family=Nunito:300,300i,400,600,700|Quicksand:300,400,700' rel='stylesheet'> 
		
		<!-- themify icon -->
		<link href='assets/css/themify-icons.css' rel='stylesheet'>
		
		<!-- animate -->
		<link href='assets/css/animate.css' rel='stylesheet'>

		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>

		
		<!-- fevicon-->
		<link rel='shortcut icon' type='image/png' href='assets/images/fevicon.png'>
		
		<!-- style -->
		<link href='style.css' rel='stylesheet'>

		<!-- style Tableau -->
		<link href='styleTableau.css' rel='stylesheet'>

 
</head>
<body>

	<div class='container '>	
		<div class='section_title text-center'>
				<h2>valeur nutritionnelle légumes</h2>

		</div>
		<div class='row col-12'>
			<img src='image/pub.png' height='100px' width='100%' alt='pub'>
		</div>
		<div id='Resultat'>";

			include'resultat-table.php';
		echo"</div>

  <div class='row justify-content-md-center'>
		  <button id='button-fruit' type='button' class='btn btn-info mr-1' onclick='afficherTab(this);'>Fruits</button>
		  <button id='button-legume' type='button' class='btn btn-info' onclick='afficherTab(this);'>Legumes</button>
 </div>
				<div id='Tableau'>
				</div>
	<!-- Fucntion -->
		<script src='main.js'></script>
		<script src='ajax.js'></script>
	<!-- jquery -->
		<script src='assets/js/jquery-3.2.1.min.js'></script>
	<script>
	</script> 
	<script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
	<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>
</body>
</html>";
