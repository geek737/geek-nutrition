<?php
	
	if(isset($_POST["Envoyer"])){
		if(isset($_POST["height"])&&isset($_POST["weight"])&&isset($_POST["age"])&&
			isset($_POST["sexe"])){
			$height=$_POST["height"];
			$weight=$_POST["weight"];
			$age=$_POST["age"];
			$sexe=$_POST["sexe"];
			if(!empty($height)&&!empty($weight)&&!empty($age)&&!empty($sexe)){
				include "function.php";
				$function_calc=new Function_calcul($height,$weight,$age,$sexe);
				//echo($function_calc->Calcul_BMI());
				$Resultat_BMR=$function_calc->Calcul_BMR();
				$Resultat_BMI=$function_calc->Calcul_BMI();
				$table = $function_calc->BMI_message($Resultat_BMI);
				$Message_BMI=$table["message"];
				$Color_BMI=$table["color"];
			}
			else
				header("location:BMI.html");
		}
		else
			header("location:BMI.html");
	}
	else
		header("location:BMI.html");
echo"<!DOCTYPE html>
<html>
	<head>
		<title>BMI</title>
		<meta charset='utf-8'>
				<!-- fonts -->
			<link href='https://fonts.googleapis.com/css?family=Nunito:300,300i,400,600,700|Quicksand:300,400,700' rel='stylesheet'> 
			
			<!-- themify icon -->
			<link href='assets/css/themify-icons.css' rel='stylesheet'>
			
			<!-- animate -->
			<link href='assets/css/animate.css' rel='stylesheet'>
			
			<!-- bootstrap -->
			<link href='assets/css/bootstrap.min.css' rel='stylesheet'>
			
			<!-- fevicon-->
			<link rel='shortcut icon' type='image/png' href='assets/images/fevicon.png'>
			
			<!-- style -->
			<link href='style.css' rel='stylesheet'>

			<!-- chartjs -->
			<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js'></script>
			
			<!-- jQuery -->
			<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
	</head>
	<body>
			<div class='container'>
				<div class='section_title text-center'>
					<h2>Statistiques</h2>
				</div>
				<div class='alert alert-info text-center' id=resultat_bmi>
					Votre BMI est : $Resultat_BMI<br>
					<div style='color:$Color_BMI;'>
						<strong>$Message_BMI</strong>
					</div>
				</div>
				<div class='alert alert-success text-center' id=resultat_bmr>
					<div>Métabolisme basale: $Resultat_BMR/kilocalories</div>
				</div>
				<canvas class='alert alert-info text-center' id=graph_bmr style='display: block;width: 100%;height: 330px;'>
				</canvas>
				<!-- Info dépense énergétique par type d'exercice -->
				 <div class='alert alert-info text-center' id=info_bmr>
				 	<div class='table-responsive text-nowrap'>
				        <!--Table-->
				        <table class='table table-striped text-center'>

				          <!--Table head-->
				          <thead >
				            <tr>
				              <th class='text-center'>niveau d'activité</th>
				              <th class='text-center'>Signification</th>
				            </tr>
				          </thead>
				          <!--Table head-->

				          <!--Table body-->
				          <tbody>
				            <tr>
				              <td >Sédentaire</td>
				              <td >peu ou aucun exercice</td>
				            </tr>
				            <tr>
				              <td>Légèrement actif</td>
				              <td>exercice léger 1 à 3 fois par semaine</td>
				            </tr>
				            <tr>
				              <td>Actif</td>
				              <td>Exercice modéré 3 à 5 fois par semaine</td>
				            </tr>
				            <tr>
				              <td>Très actif</td>
				              <td>Exercice Hard 6 à 7 fois par semaine</td>
				            </tr>
				            <tr>
				              <td>Extrêmement actif</td>
				              <td>Exercice très dur sport et travail physique ou s'entraîner 2 fois par jour</td>
				            </tr>
				          </tbody>
				          <!--Table body-->


				        </table>
				        <!--Table-->
     				</div>
				</div>
			</div>
			<script src='assets/js/Chart.min.js'></script>
			<script>
			    let myChart = document.getElementById('graph_bmr').getContext('2d');

			    // Global Options
			    //Chart.defaults.global.defaultFontFamily = 'Lato';
			    Chart.defaults.global.defaultFontSize = 14;
			    Chart.defaults.global.defaultFontColor = '#777';

			    let massPopChart = new Chart(myChart, {
			      type:'horizontalBar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
			      data:{
			        labels:['Métabolisme basal', 'Sédentaire', 'Légèrement actif', 'Actif', 'Très actif', 'Extrêmement actif'],
			        datasets:[{
			          label:'Calories',
			          data:[
			            Math.round($Resultat_BMR*100)/100,
			            Math.round(($Resultat_BMR*1.2)*100)/100,
			            Math.round(($Resultat_BMR*1.375)*100)/100,
			            Math.round(($Resultat_BMR*1.55)*100)/100,
			            Math.round(($Resultat_BMR*1.725)*100)/100,
			            Math.round(($Resultat_BMR*1.9)*100)/100
			          ],
			          backgroundColor:'#FF6F91',
			          borderWidth:1,
			          borderColor:'#777',
			          hoverBorderWidth:3,
			          hoverBorderColor:'#000'
			        }]
			      },
			      options:{
			        title:{
			          display:true,
			          text:'Votre besoin d energie par jour',
			          fontSize:25
			        },
			        legend:{
			          display:true,
			          position:'right',
			          labels:{
			            fontColor:'#000'
			          }
			        },
			        layout:{
			          padding:{
			            left:50,
			            right:0,
			            bottom:0,
			            top:0
			          }
			        },
			        tooltips:{
			          enabled:true
			        }
			      }
			    });
  </script>
	</body>
</html>";